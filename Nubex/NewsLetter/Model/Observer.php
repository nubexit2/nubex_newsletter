<?php
namespace Nubex\NewsLetter\Model;
class Observer
{
    protected $_queueCollectionFactory;

    /**
     * Construct
     *
     * @param \Magento\Newsletter\Model\ResourceModel\Queue\CollectionFactory $queueCollectionFactory
     */
    public function __construct(
        \Magento\Newsletter\Model\ResourceModel\Queue\CollectionFactory $queueCollectionFactory
    ) {
        $this->_queueCollectionFactory = $queueCollectionFactory;
    }
    
    public function aroundScheduledSend(
        \Magento\Newsletter\Model\Observer $subject,
        \Closure $proceed
    ) {
        $countOfQueue  = 3;
        $countOfSubscriptions = 1000;

        /** @var \Magento\Newsletter\Model\ResourceModel\Queue\Collection $collection */
        $collection = $this->_queueCollectionFactory->create();
        $collection->setPageSize($countOfQueue)->setCurPage(1)->addOnlyForSendingFilter()->load();

        $collection->walk('sendPerSubscriber', [$countOfSubscriptions]);
    }
}
